[![pipeline status](https://gitlab.com/2009scape/rs09-thanos-tool/badges/master/pipeline.svg)](https://gitlab.com/2009scape/rs09-thanos-tool/-/commits/master)

**Zaros : All-in-one JSON editing**

This tool is capable of modifying every JSON file you're likely to need to modify inside of the 2009scape codebase.

This tool is intended only for developers of 2009scape. 

If you are running your own fork of the codebase please do not report your bugs to me.
